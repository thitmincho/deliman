var app = angular.module('starter.controllers', ['ngMap','ngCordova'])


        .controller('AppCtrl', function ($scope,$interval,$cordovaGeolocation, $ionicPlatform,LocalStorage,$http,BASEURL,$ionicModal, $timeout, $state, $window, $rootScope,$cordovaBarcodeScanner,$ionicPopup) {


    //
// With the new view caching in Ionic, Controllers are only called
            // when they are recreated or on app start, instead of every page change.
            // To listen for when this page is active (for example, to refresh data),
            // listen for the $ionicView.enter event:
            //$scope.$on('$ionicView.beforeEnter', function(e) {
            //});
//$rootScope.user_id;
            /*if($rootScope.user_id==undefined)
             {
             $rootScope.user_id=false;
             }*/


            var backbutton = 0;
            $ionicPlatform.registerBackButtonAction(function (e) {
                e.preventDefault();


            $scope.today_date = new Date();


                if (backbutton == 0) {


                    backbutton++;
                    window.plugins.toast.showShortCenter('Press again to exit');
                    $timeout(function () { backbutton = 0; }, 5000);
                }
                else  if (backbutton == 1) {
                  ionic.Platform.exitApp();
                }
    else
    {
      ionic.Platform.exitApp();
     }


            }, 1000);



            $scope.logout = function (event) {
                event.preventDefault();
                $rootScope.user_id = false;
                LocalStorage.setData(false);
                $window.sessionStorage["userInfo"] = false;
                $state.go('auth.login');
            }
            $scope.logged_in_user = LocalStorage.getData();


            // Form data for the login modal
            $scope.loginData = {};

            // Create the login modal that we will use later
            $ionicModal.fromTemplateUrl('templates/login.html', {
                scope: $scope
            }).then(function (modal) {
                $scope.modal = modal;
            });

            // Triggered in the login modal to close it
            $scope.closeLogin = function () {
                $scope.modal.hide();
            };

//            // Open the login modal
//            $scope.login = function () {
//                $scope.modal.show();
//            };
//
//            // Perform the login action when the user submits the login form
//            $scope.doLogin = function () {
//                console.log('Doing login', $scope.loginData);
//
//                // Simulate a login delay. Remove this and replace with your login
//                // code if using a login system
//                $timeout(function () {
//                    $scope.closeLogin();
//                }, 1000);
//            };

            $interval(function() {
               $scope.getUserLatLong();
            }, 5000);
           /* */

         $scope.scan = function() {


	   cordova.plugins.barcodeScanner.scan(
      function (result) {

		  if(result.cancelled==false)
		  {
			     var current_user = JSON.parse($window.sessionStorage["userInfo"]);
		   $http({

                            url: BASEURL + '/api.php?method_name=scanAwb',
                            method: "POST",
                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                            data: {user_id:current_user[0].id ,awb:result.text}
                        }).success(function (response) {
							//alert(response.type)
							if(response.type=='P')
							{
								 $state.go('app.booking-detail',{id:result.text});
								}
							else if(response.type=='D')
							{
								 $state.go('app.drs-booking-detail',{id:result.text});
								}
								else
								{
									 $ionicPopup.alert({
                          			 title: 'Error!',
                              		 subTitle: 'Shipment does not belongs to you.',
	                            });

									}

                        });

			}

         /* alert("We got a barcode\n" +
                "Result: " + result.text + "\n" +
                "Format: " + result.format + "\n" +
                "Cancelled: " + result.cancelled);*/
      },
      function (error) {
          $ionicPopup.alert({
                          			 title: 'Error!',
                              		 subTitle: 'there is some hardware problem!',
	                            });
      },
      {
          preferFrontCamera : false, // iOS and Android
          showFlipCameraButton : true, // iOS and Android
          showTorchButton : true, // iOS and Android
          torchOn: false, // Android, launch with the torch switched on (if available)
          prompt : "Place a barcode inside the scan area", // Android
          resultDisplayDuration: 500, // Android, display scanned text for X ms. 0 suppresses it entirely, default 1500
          formats : "QR_CODE,PDF_417", // default: all but PDF_417 and RSS_EXPANDED
          orientation : "portrait|landscape", // Android only (portrait|landscape), default unset so it rotates with the device
          disableAnimations : true, // iOS
          disableSuccessBeep: false // iOS
      }
   );
        };
         $scope.getUserLatLong = function(){
                var current_user = JSON.parse($window.sessionStorage["userInfo"]);

                if (current_user != undefined && current_user != false)
                {
                    var posOptions = {timeout: 5000, enableHighAccuracy: true}
                    $cordovaGeolocation.getCurrentPosition(posOptions)
                    .then(function(position){
                        var lat  = position.coords.latitude
                        var long = position.coords.longitude
                        $scope.my_lat = lat;
                        $scope.my_long = long;

                        $http({
                            url: BASEURL + '/api.php?method_name=save_lat_long',
                            method: "POST",
                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                            data: {user_id:current_user[0].id ,lat:lat,lng:long}
                        }).success(function (response) {

                        });
                    }, function(error){
                        console.log('error:', error);
                    });
                }
            }
        })




//        .controller('LoginCtrl', function ($scope,$ionicPopup, $q,$ionicLoading, $rootScope,$ionicBackdrop, $http, BASEURL, $window, $state) {
//            $ionicBackdrop.release();
//            $scope.user = {
//                email: '',
//                password: ''
//            }
//
//            $scope.current_user = JSON.parse($window.sessionStorage["userInfo"]);
//
//                $scope.authenticate = function (event, user) {
//                    event.preventDefault();
//                    $ionicLoading.show({
//                        template: '<ion-spinner icon="ios"></ion-spinner>'
//                    });
//                    var deferred = $q.defer();
//                    $http({
//                        url: BASEURL + '/user.php?method_name=login',
//                        method: "POST",
//                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
//                        data: user
//                    }).success(function (response) {
//                        $ionicLoading.hide();
//                        if (!response)
//                        {
//                            $ionicPopup.alert({
//                                title: 'Error!',
//                                subTitle: 'User name or password is invalid.',
//                            });
//                        } else
//                        {
//                            $window.sessionStorage["userInfo"] = JSON.stringify(response);
//                            deferred.resolve(response);
//                            $state.go('app.profile');
//                        }
//                    });
//                }
//
//        })

        .controller('RegisterCtrl', function ($scope,$ionicLoading,$ionicPopup,$http, BASEURL, $window, $state, UserService) {
            $scope.user = {
                id: '',
                messenger_name: '',
                mobile: '',
                vehicle_number: '',
                email: '',
                password: '',
                confirm_password: ''
            };


            $scope.register = function (event, user) {
                event.preventDefault();
                $ionicLoading.show({
                    template: '<ion-spinner icon="ios"></ion-spinner>'
                });
                $http({
                    url: BASEURL + '/api.php?method_name=register',
                    method: "POST",
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    data: user
                }).success(function (response) {
                    $ionicLoading.hide();
                    if (response.status == 0)
                    {
                        $ionicPopup.alert({
                                title: 'Error!',
                                subTitle: 'Invalid form data.',
                        });
                    } else
                    {
                        $state.go('auth.login');
                    }
                });
            }
        })
//        .controller('ProfileCtrl', function ($scope, $window, $http, BASEURL, $state, UserService) {
//            $scope.current_user = JSON.parse($window.sessionStorage["userInfo"]);
//            $scope.current_user[0].confirm_password = '';
//            $scope.current_user[0].country = COUNTRY;
//
//            if ($scope.current_user[0].id == undefined)
//            {
//                $state.go('auth.login');
//            }
//
//            //console.log($scope.current_user[0].id)
////       $scope.getCountryList = function(){
////           UserService.getCountryList().success(function(response){
////               $scope.countries = response;
////           });
////       }
//
//            $scope.getCityList = function () {
//                UserService.getCityList(COUNTRY).success(function (response) {
//                    $scope.cities = response;
//                });
//            }
//
//            $scope.profileDetail = function (event) {
//                $http({
//                    url: BASEURL + "/user.php?method_name=user_detail&id=" + $scope.current_user[0].id,
//                    method: "POST",
//                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
//                    data: $scope.current_user
//                }).success(function (response) {
//                    console.log(response)
//                    $scope.user_detail = response;
//                });
//            };
//
//
//
//            $scope.updateProfile = function (event) {
//                event.preventDefault();
//                $http({
//                    url: BASEURL + '/user.php?method_name=register',
//                    method: "POST",
//                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
//                    data: $scope.user_detail
//                }).success(function (response) {
//                    if (response.status == 0)
//                    {
//                        $scope.errors = response.errors;
//                    } else
//                    {
//                        $scope.errors = '';
//                        $scope.message = '!Profile has been updated successfully.';
//                    }
//                });
//            }
//        })


        .controller('BookingListCtrl', function ($scope,$state,$ionicLoading, $http, BASEURL, $window) {


          $ionicLoading.show({
                template: '<ion-spinner icon="ios"></ion-spinner>'
            });


            $scope.$on('$ionicView.beforeEnter', function() {
                $scope.current_user = JSON.parse($window.sessionStorage["userInfo"]);
                $scope.pickup_list = {
                user_id: $scope.current_user[0].id,
                method_name: 'pickup_list',
                search:''
            };

                // Code you want executed every time view is opened
                $scope.my_pickups = [];
                $scope.page_no = 1;
                if($state.current.name == 'app.pickup-history'){
                    $scope.getPickupHistoryList();
                }else{
                    $scope.getPickupList();
                }
             });

            $scope.getPickupList = function () {
                $scope.pickup_list.user_id = $scope.current_user[0].id;
                $http({
                    url: BASEURL + '/api.php?page='+$scope.page_no+'&method_name=pickup_list',
                    method: "POST",
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    data: $scope.pickup_list
                }).success(function (response) {
                    if(response.length > 0){

                        angular.forEach(response,function(value){
                            console.log(value)
                            if(value.delivered == 'N'){
                                $scope.my_pickups.push(value);
                            }
                        });
                        $scope.$broadcast('scroll.infiniteScrollComplete');
                    }else{
                       $scope.stopLoader = true;
                    }
                    $ionicLoading.hide();
                });
            }

            $scope.loadMore = function(){
                $scope.page_no += 1;
                $scope.getPickupList();
            }


            $scope.getPickupHistoryList = function () {
                $scope.pickup_list.user_id = $scope.current_user[0].id;
                $http({
                    url: BASEURL + '/api.php?page='+$scope.page_no+'&method_name=pickup_history_list',
                    method: "POST",
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    data: $scope.pickup_list
                }).success(function (response) {
                    if(response.length > 0){
                        angular.forEach(response,function(value){
                            if(value.delivered == 'Y'){
                                $scope.my_pickups.push(value);
                            }
                        });
                        $scope.$broadcast('scroll.infiniteScrollComplete');
                    }else{
                       $scope.stopLoader = true;
                    }

                    $ionicLoading.hide();
                });
            }

            $scope.loadMoreHistory = function(){
                $scope.page_no += 1;
                $scope.getPickupHistoryList();
            }


            $scope.searchPickupList = function(event){
                event.preventDefault();
                $scope.my_pickups = [];
                $scope.page_no = 1;
                $ionicLoading.show({
                    template: '<ion-spinner icon="ios"></ion-spinner>'
                });
                $scope.getPickupList();
            }


            $scope.searchPickupHistoryList = function(event){
                event.preventDefault();
                $scope.my_pickups = [];
                $scope.page_no = 1;
                $ionicLoading.show({
                    template: '<ion-spinner icon="ios"></ion-spinner>'
                });
                $scope.getPickupHistoryList();
            }
        })


       .controller('PickupDetailCtrl', function ($scope,$ionicLoading, $http, BASEURL, $window, $stateParams) {
            $ionicLoading.show({
                template: '<ion-spinner icon="ios"></ion-spinner>'
            });


            $scope.$on('$ionicView.beforeEnter', function() {
                $scope.search = '';
            $scope.awb_list = [];
            $scope.page_no = 1;

            $scope.current_user = JSON.parse($window.sessionStorage["userInfo"]);
               if ($stateParams.id) {
                   $scope.pickup_detail = {
                       booking_id: $stateParams.id,
                       user_id: $scope.current_user[0].id
                   };
               }
                $scope.getPickupDetail();
            });



//            $scope.$on('$ionicView.beforeEnter', function() {
//                $scope.search = '';
//                $scope.awb_list = [];
//                $scope.page_no = 1;
//                // Code you want executed every time view is opened
//                 $scope.getPickupDetail();
//             });
            $scope.getPickupDetail = function () {

                $http({
                    url: BASEURL + '/api.php?page='+$scope.page_no+'&search='+$scope.search+'&pickup_id=' + $stateParams.id + '&type='+$stateParams.type+'&method_name=pickup_detail&user_id=' + $scope.current_user[0].id,
                    method: "GET",
                    data: $scope.pickup_detail
                }).success(function (response) {
                    if(response.length > 0){
                        angular.forEach(response,function(value){
                           $scope.awb_list.push(value);
                        });
                        $scope.$broadcast('scroll.infiniteScrollComplete');
                    }else{
                       $scope.stopLoader = true;
                    }
                    $ionicLoading.hide();
                });
            }
            $scope.loadMore = function(){
				 $scope.stopLoader = false;
                $scope.page_no += 1;
                $scope.getPickupDetail();
            }
        })

.controller('SignCtrl', function ($scope, $http, BASEURL,$state,$cordovaCamera, $cordovaFile, $window, $stateParams,$ionicPopup,$ionicLoading) {
                var canvas = document.getElementById('signatureCanvas');

                var signaturePad = new SignaturePad(canvas);


                $scope.clearCanvas = function() {
                    signaturePad.clear();
                }

                $scope.sign = {};
                $scope.saveCanvas = function() {
                    var confirmPopup = $ionicPopup.confirm({
                    title: 'Are you sure?'
                  });

                  confirmPopup.then(function(res) {
                      if(res) {
                            $ionicLoading.show({
                                template: '<ion-spinner icon="ios"></ion-spinner>'
                            });
                            var sigImg = signaturePad.toDataURL();
                            $scope.sign.sign_image = sigImg;
                                $http({
                                    url: BASEURL + '/api.php?method_name=take_sign&drs_unique_id='+$stateParams.id,
                                    method: "POST",
                                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    data: $scope.sign
                                }).success(function (response) {
                                    $ionicLoading.hide();
                                    $ionicPopup.alert({
                                            title: 'Success!'
                                    });
                                    $state.go('app.dashboard',{id:$stateParams.id});
                            });
                      }

                  });


                }



      /////////////////////////////////////////////////////////////////////
      ////////////////////////////////////////////////////////////////////

        $scope.takePhoto = function () {
            document.addEventListener("deviceready", function () {
            var options = {
                quality: 20,
                destinationType: Camera.DestinationType.DATA_URL,
                sourceType: Camera.PictureSourceType.CAMERA,
                allowEdit: false,
                encodingType: Camera.EncodingType.PNG,
                targetWidth: 200,
                targetHeight: 200,
                popoverOptions: CameraPopoverOptions,
                saveToPhotoAlbum: false
            };
            $cordovaCamera.getPicture(options).then(function (imageData) {
                $scope.srcImage = "data:image/png;base64," + imageData;
                $http({
                        url: BASEURL + '/api.php?method_name=upload_receiver_photo&drs_unique_id='+$stateParams.id,
                        method: "POST",
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                        data: {receiver_image:$scope.srcImage}
                }).success(function (response) {
                        $ionicPopup.alert({
                                title: 'Success!'
                        });
                });
            }, function (err) {
                $ionicPopup.alert({
                        title: err
                });
            });
            }, false);

        }
//
        $scope.choosePhoto = function (){
                document.addEventListener("deviceready", function () {
                var options = {
                    quality: 100,
                    destinationType: Camera.DestinationType.DATA_URL,
                    sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                    allowEdit: false,
                    encodingType: Camera.EncodingType.PNG,
                    targetWidth: 800,
                    targetHeight: 1100,
                    popoverOptions: CameraPopoverOptions,
                    saveToPhotoAlbum: false
                };
                $cordovaCamera.getPicture(options).then(function (imageData) {
                    $scope.srcImage = "data:image/png;base64," + imageData;
                    $http({
                            url: BASEURL + '/api.php?method_name=upload_receiver_photo&drs_unique_id='+$stateParams.id,
                            method: "POST",
                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                            data: {receiver_image:$scope.srcImage}
                    }).success(function (response) {
                            $ionicPopup.alert({
                                    title: 'Success!'
                            });
                    });
                }, function (err) {
                    $ionicPopup.alert({
                            title: err
                    });
                });
                }, false);
        }



      ////////////////////////////////////////////////////////////////////
      ////////////////////////////////////////////////////////////////////


        })


        .controller('BookingDetailCtrl', function ($scope,$ionicLoading,$cordovaGeolocation, $http, BASEURL, $window, $stateParams,$ionicPopup,$state,$q) {
            $scope.current_user = JSON.parse($window.sessionStorage["userInfo"]);

          $scope.$on('$ionicView.beforeEnter', function() {
            // Code you want executed every time view is opened
             //$scope.getBookingDetail();//yuvraj

         })

//================================================================================new working for new status===============================================================
		  $scope.updatedata={
				coment_status:'',
				status:'',
				slipno:''

				}
		  $scope.test = function (event) {
				event.preventDefault();
			 $scope.updatedata.slipno=	$stateParams.id ;
                var confirmPopup = $ionicPopup.confirm({
                    title: 'Are you sure?'
                  });

				console.log($scope.updatedata.status)

			   if($scope.updatedata.status=='D')
			   {

				    $state.go('app.signature-pad', {id:$stateParams.id });
				   }else
				   {
			   		 confirmPopup.then(function(res) {
                    if(res) {
                   $http({
                    url: BASEURL + '/api.php?&method_name=get_status',
                    method: "POST",
					 headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    data: $scope.updatedata
                }).success(function (response) {
					$ionicPopup.alert({
                                            title: 'Success! Status Updated Successfully',
                                });

					 $state.go('app.dashboard');
                });

				   }
			});

				   }

            }

//===========================================================================end new working for new status===============================================================


            $scope.getBookingDetail = function () {
                $http({
                    url: BASEURL + '/api.php?booking_id=' + $stateParams.id + '&method_name=booking_detail&user_id=' + $scope.current_user[0].id,
                    method: "GET",
                    data: $scope.booking
                }).success(function (response) {
                    $scope.booking_detail = response;
                });
            }


			 $scope.getreasiondropfordelay = function () {
                 var checkbox = element(by.model('checked'));
				  var checkElem = element(by.css('.check-element'));

				  expect(checkElem.isDisplayed()).toBe(false);
				  checkbox.click();
				  expect(checkElem.isDisplayed()).toBe(true);
            }


            $scope.changeStatus = function(event){
                event.preventDefault();
                var confirmPopup = $ionicPopup.confirm({
                    title: 'Are you sure?'
                  });

                  confirmPopup.then(function(res) {
                    if(res) {
                        $http({
                                url: BASEURL + "/api.php?method_name=pickup_change_status&slip_no=" + $scope.booking_detail.slip_no,
                                method: "GET"
                        }).success(function (response) {
                                $ionicPopup.alert({
                                            title: 'Success!',
                                });
                                 $scope.getBookingDetail();
                        });
                    }
                });

            }


             $scope.updateLocation = function (event) {
                event.preventDefault();

                $ionicLoading.show({
                    template: '<ion-spinner icon="ios"></ion-spinner>'
                });


                 var posOptions = {timeout: 5000, enableHighAccuracy: true}
                    $cordovaGeolocation.getCurrentPosition(posOptions)
                    .then(function(position){
                        var lat  = position.coords.latitude;
                        var long = position.coords.longitude;
                        $http({
                               url: BASEURL + '/api.php?method_name=update_location&lat='+lat+'&long='+long+'&cust_id=' + $scope.booking_detail.cust_id,
                               method: "GET"
                        }).success(function (response){
                            $ionicPopup.alert({
                                title: 'Success!',
                                subTitle: '!Location has been updated successfully.',
                            });
                            $scope.getBookingDetail();
                            $ionicLoading.hide();
                        });
                    }, function(error){
                        $ionicPopup.alert({
                                title: error
                        });
                    });




            }

            $scope.accept_delivery = function (slip_no) {
                $http({
                    url: BASEURL + '/api.php?method_name=accept_delivery&order_id='+slip_no,
                    method: "GET",
                    data: $scope.booking
                }).success(function (response) {

                    //$scope.booking_detail = response;
                    $scope.getBookingDetail();
                });
            }

            $scope.reject_delivery = function (slip_no) {
                $http({
                    url: BASEURL + '/api.php?method_name=reject_delivery&order_id='+slip_no,
                    method: "GET",
                    data: $scope.booking
                }).success(function (response) {

                    //$scope.booking_detail = response;
                    $state.go('app.dashboard');
                });
            }
            $scope.reject_delivery1 = function (slip_no) {
                $http({
                    url: BASEURL + '/api.php?method_name=reject_delivery1&order_id='+slip_no,
                    method: "GET",
                    data: $scope.booking
                }).success(function (response) {

                    //$scope.booking_detail = response;
                    $state.go('app.dashboard');
                });
            }




           $scope.openLocation = function (address) {
            //window.open(‘geo:lat,lon?q=address’, ‘_system’);

            var posOptions = {timeout: 5000, enableHighAccuracy: true}
            $cordovaGeolocation.getCurrentPosition(posOptions)
            .then(function(position){
                var lat  = position.coords.latitude
                var long = position.coords.longitude


                //window.open('geo:'+lat+','+long+'?q='+$scope.booking_detail[0].reciever_address, '_system');
                //window.open('maps://?q=' +lat+','+long, '_system');
                if (ionic.Platform.isIOS())
                  window.open("http://maps.apple.com/?q="+$scope.booking_detail[0].reciever_address+"&ll="+lat+","+long+"&near=#{lat},#{long}", '_system', 'location=yes')
                else{
                  window.open("http://maps.google.com/maps?saddr=current+location&daddr="+address+"&mode=driving", '_system', 'location=yes');
                  //window.open('maps://?q=' +lat+','+long+'&q='+$scope.booking_detail[0].reciever_address, '_system');
                }

                //window.open('maps://?q='+address, '_system');

            }, function(error){
                console.log('error-geo:', error+"lat"+lat+",lang"+lang);
            });



          }



        })


        .controller('NewsCtrl', function ($scope,$ionicLoading, $state,$http, BASEURL, $window) {

		          // $ionicLoading.show({
             //    template: '<ion-spinner icon="ios"></ion-spinner>'
            //});
            var current_user = JSON.parse($window.sessionStorage["userInfo"]);
                $http({
                    url: BASEURL + '/api.php?method_name=news_list&user_id='+current_user[0].id,
                    method: "GET",
                    data: $scope.booking_detail
                }).success(function (response) {
					//$ionicLoading.hide();
                    $scope.news_list = response;
                });

            /*
            if ($stateParams.id) {
                $http({
                    url: BASEURL + "/api.php?method_name=news_detail&news_id=" + $stateParams.id,
                    method: "GET",
                    data: $scope.booking_detail
                }).success(function (response) {
                    $scope.news_detail = response;
                });
            }
            */


        })
        .controller('ContactUsCtrl', function ($scope, $http, BASEURL) {

            $scope.submitForm = function (event, contact_us) {
                event.preventDefault();
                $http({
                    url: BASEURL + '/api.php?method_name=contact_us',
                    method: "POST",
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    data: contact_us
                }).success(function (response) {
                    if (response.status == 0) {
                        $scope.errors = response.errors;
                        $scope.message = '';
                    } else {
                        $scope.errors = '';
                        $scope.contact_us = {
                            name: '',
                            email: '',
                            mobile: '',
                            message: '',
                            subject: ''
                        };
                        $scope.message = response;
                    }
                });
            }

        })
        .controller('ForgotPasswordCtrl', function ($scope, $http, BASEURL) {

            $scope.submitForm = function (event, forgot_password) {
                event.preventDefault();
                $scope.forgot_password = {
                    email: ''
                };
                $http({
                    url: BASEURL + '/api.php?method_name=forgot_password',
                    method: "POST",
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    data: forgot_password
                }).success(function (response) {
                    if (response.status == 0) {
                        $scope.error = response.error;
                        $scope.message = '';
                    } else {
                        $scope.message = 'An email has been suucessfully sent to your account.';
                        $scope.error = '';
                        $scope.forgot_password = {
                            email: ''
                        };
                    }
                });
            }

        })
        .controller('trackCltr', function ($scope, $http,BASEURL,$state,$stateParams,TrackingService) {
            /* var base_url =  "http://www.alshrouqexpress.com";

             var user_name="firstapitest_user";
             var uniqueId="5f4dcc3b5aa765d61d8327deb882cf99";
             var password="password";
             */

             $scope.error = '';

            //var base_url =  "http://192.168.1.10/alshrouqexpress_v6";
            //var user_name="vijay";
            //var uniqueId="AM3705CGV3JZAJUMA30QCU94DDB6US";
            //var password="4UXYMQW1V";
            $scope.tracking = {tracking_no:''};

            $scope.trackingFunction = function (event,tracking) {
                event.preventDefault();
                TrackingService.getTrackingDetail(tracking).success(function (response) {
                    if(response.status == 0){
                        $scope.error = response.error;
                    }else{
                        $scope.error = '';
                        $state.go('app.tracking-detail', {tracking_no:tracking.tracking_no});
                    }
                });
            }

            if ($stateParams.tracking_no) {
                $scope.tracking.tracking_no = $stateParams.tracking_no;
                TrackingService.getTrackingDetail($scope.tracking).success(function (response) {
                    if(response.status == 0){
                        $scope.error = response.error;
                    }else{
                        $scope.error = '';
                        $scope.tracking_detail = response;
                    }
                });
            }


        })


/*----------------------------------------------------------------------------
 * controller for frontend
 ------------------------------------------------------------------------*/

.controller('HomeCtrl',function($scope,LocalStorage,$ionicLoading,$http,BASEURL){
//    $ionicLoading.show({
//        template: '<ion-spinner icon="ios"></ion-spinner>'
//    });
    $scope.getHomepageItems = function(){
        $http({
            url: BASEURL + '/api.php?method_name=homepage',
            method: "GET"
        }).success(function (response) {
            $scope.categories = response;
            $ionicLoading.hide();
        });
    }

})

.controller('newpageCltr',function($scope,LocalStorage,$ionicLoading,$http,BASEURL){
//    $ionicLoading.show({
//        template: '<ion-spinner icon="ios"></ion-spinner>'
//    });
    $scope.getHomepageItems = function(){
        $http({
            url: BASEURL + '/api.php?method_name=homepage',
            method: "GET"
        }).success(function (response) {
            $scope.categories = response;
            $ionicLoading.hide();
        });
    }

});


app.controller('CategoryCtrl',function($scope,$ionicLoading,$http,BASEURL,$stateParams){
    $ionicLoading.show({
                    template: '<ion-spinner icon="ios"></ion-spinner>'
    });

    if ($stateParams.id) {
        var id = $stateParams.id;
        $http({
            url: BASEURL +'/api.php?method_name=category_detail&cat_id='+id,
            method: "GET"
        }).success(function (response) {
            $scope.cat_detail = response.cat;
            $scope.sub_categories = response.sub_cat;
            $ionicLoading.hide();
        });
    }

});

app.controller('SubCategoryCtrl',function($scope,$ionicLoading,$http,BASEURL,$stateParams){
    $ionicLoading.show({
                    template: '<ion-spinner icon="ios"></ion-spinner>'
    });

    if ($stateParams.id) {
        var id = $stateParams.id;
        $http({
            url: BASEURL +'/api.php?method_name=subcategory_detail&cat_id='+id,
            method: "GET"
        }).success(function (response) {
            $scope.cat_detail = response.cat;
            $scope.sub_category = response.sub_cat;
            $ionicLoading.hide();
        });
    }

});

app.controller('OrderCtrl',function($scope,$window,$ionicPopup,$ionicLoading,$state,$http,BASEURL,$stateParams){
    $ionicLoading.show({
                    template: '<ion-spinner icon="ios"></ion-spinner>'
    });

    $scope.buy_now_detail = {
        user_name:'',
        mobile:'',
        address:''
    }

    if ($stateParams.id) {

       if($window.sessionStorage["userInfo"] == undefined){
            $ionicLoading.hide();
            $state.go('auth.login');
       }else{
            var current_user = JSON.parse($window.sessionStorage["userInfo"]);
            if(current_user == 'false'){
               $state.go('auth.login');
            }else{
                $scope.current_user = current_user;
                var id = $stateParams.id;
                 $scope.buy_now_detail.cat_id = id;
                 $scope.buy_now_detail.user_id = current_user[0].id;
                 $scope.buy_now_detail.user_name = current_user[0].user_name;
                 $scope.buy_now_detail.mobile = current_user[0].user_mobile;
                 $scope.buy_now_detail.address = current_user[0].address;
                 $http({
                     url: BASEURL +'/api.php?method_name=subcategory_detail&cat_id='+id,
                     method: "GET"
                 }).success(function (response) {
                     $scope.cat_detail = response.cat;
                     $scope.sub_category = response.sub_cat;
                     $ionicLoading.hide();
                 });
            }
       }
    }


    $scope.buynow = function(event,order_info){
        event.preventDefault();
        $http({
            url: BASEURL + '/api.php?method_name=buy_now',
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: order_info
        }).success(function (response) {
                if(response.status == 0){
                    $ionicPopup.alert({
                        title: 'Error!',
                        subTitle: 'Invalid input data.',
                    });
                }else{
                    $ionicPopup.alert({
                        title: 'Success!',
                        subTitle: 'Your order has been successfully placed.',
                    });
                }
                $ionicLoading.hide();
        });
    }


});



app.controller('LoginCtrl', function ($scope,LocalStorage, $q,$ionicPopup, $rootScope,$ionicLoading,$ionicBackdrop, $http, BASEURL, $window, $state,$cordovaBarcodeScanner) {



			$ionicBackdrop.release();
            $scope.user = {
                email: window.localStorage['email'],
                password: window.localStorage['password']
            }


            $scope.authenticate = function () {
                $ionicLoading.show({
                    template: '<ion-spinner icon="ios"></ion-spinner>'
                });

                var deferred = $q.defer();
                $http({
                   url: BASEURL+'/api.php?method_name=login',
                    method: "POST",
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    data:   $scope.user
                }).success(function (response){
                    if (!response)
                    {
                        $ionicPopup.alert({
                            title: 'Error!',
                            subTitle: 'User name or password is invalid.',
                        });
                    } else
                    {
						console.log(response);
                        $scope.error = '';
                        LocalStorage.setData(true);
                        $window.sessionStorage["userInfo"] = JSON.stringify(response);
                        //window.plugins.oneSignal.sendTag('userid', response.user.id);
                        deferred.resolve(response);

                        //window.localStorage['email'] =  $scope.user.email;
                        //window.localStorage['password'] =  $scope.user.password;
                        $state.go('app.dashboard');
                    }
                     $ionicLoading.hide();
                });
            }
            document.addEventListener('deviceready', function () {

    // Android customization
    // To indicate that the app is executing tasks in background and being paused would disrupt the user.
    // The plug-in has to create a notification while in background - like a download progress bar.
      cordova.plugins.backgroundMode.setDefaults({
          title:  'TheTitleOfYourProcess',
          text:   'Executing background tasks.'
      });

      // Enable background mode
      cordova.plugins.backgroundMode.enable();

      // Called when background mode has been activated
      cordova.plugins.backgroundMode.onactivate = function () {

          // Set an interval of 3 seconds (3000 milliseconds)
          setInterval(function () {

              // The code that you want to run repeatedly

          }, 2000);
      }
  }, false);

});

app.controller('ProfileCtrl', function ($scope,$ionicPopup,$ionicLoading, $window, $http, BASEURL, $state, UserService) {
            $scope.current_user = JSON.parse($window.sessionStorage["userInfo"]);
            $scope.$on('$ionicView.beforeEnter', function() {
                $scope.current_user = JSON.parse($window.sessionStorage["userInfo"]);
            });

            if ($scope.current_user[0].id == undefined)
            {
                $state.go('auth.login');
            }
            else{
                $scope.user_detail = $scope.current_user;
                $scope.user_detail.password = '';
//                $scope.getCityList = function () {
//                    UserService.getCityList(COUNTRY).success(function (response) {
//                        $scope.cities = response;
//                    });
//                }

//
                $scope.updateProfile = function (event) {
                    event.preventDefault();
                    alert("hello");
                    $ionicLoading.show({
                        template: '<ion-spinner icon="ios"></ion-spinner>'
                    });
                    $http({
                        url: BASEURL + '/api.php?method_name=register',
                        method: "POST",
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                        data: $scope.user_detail
                    }).success(function (response) {
                        if (response.status == 0)
                        {
                            $ionicPopup.alert({
                                title: 'Error!',
                                subTitle: 'Invalid form data.',
                            });
                        } else
                        {
                            $ionicPopup.alert({
                                title: 'Success!',
                                subTitle: '!Profile has been updated successfully.',
                            });
                        }
                        $ionicLoading.hide();
                    });
                }
            }
});


app.controller('OrderListCtrl',function($scope,$window,$ionicPopup,$ionicLoading,$state,$http,BASEURL,$stateParams){


       var current_user = JSON.parse($window.sessionStorage["userInfo"]);
       $scope.order_list = [];
       if(current_user == 'false'){
            $state.go('auth.login');
            $ionicLoading.hide();
       }else{
           $scope.page_no = 1;
           $ionicLoading.show({
                template: '<ion-spinner icon="ios"></ion-spinner>'
            });
            $scope.getOrderList = function(){
                $http({
                    url: BASEURL +'/api.php?page='+$scope.page_no+'&method_name=order_list&user_id='+current_user[0].id,
                    method: "GET"
                }).success(function (response) {
                    if(response.length > 0){
                        angular.forEach(response, function(value){
                            $scope.order_list.push(value);
                        });
                    }else{
                        $scope.stopLoader=true;
                    }
                    $scope.$broadcast('scroll.infiniteScrollComplete');
                    $ionicLoading.hide();
                });
            }

             $scope.loadMore = function(){
                $scope.page_no += 1;
                $scope.getOrderList();
            }
       }

});


app.controller('DrsListCtrl', function ($scope,$ionicLoading, $state,$http, BASEURL, $window) {




    $scope.$on('$ionicView.beforeEnter', function() {

        $scope.current_user = JSON.parse($window.sessionStorage["userInfo"]);
        //console.log(current_user);
        $scope.drs_list = {
            user_id: $scope.current_user[0].id,
            search:''
        };
        $ionicLoading.show({
            template: '<ion-spinner icon="ios"></ion-spinner>'
        });
        $scope.my_drs = [];
        $scope.page_no = 1;
        // Code you want executed every time view is opened
        if($state.current.name == 'app.delivered'){
            $scope.getDliveredList();
        }else{
            $scope.getDrsList();
        }
    });
    $scope.getDrsList = function () {
        $scope.drs_list.user_id = $scope.current_user[0].id;
        $http({
            url: BASEURL + '/api.php?page='+$scope.page_no+'&method_name=drs_list',
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: $scope.drs_list
        }).success(function (response){
            if(response.length > 0 && response != null){

                angular.forEach(response,function(value){
                    if(value.delivered == 'N'){
                        $scope.my_drs.push(value);
                    }
                });
                $scope.$broadcast('scroll.infiniteScrollComplete');
            }else{
               $scope.stopLoader = true;
            }
            $ionicLoading.hide();
        });
    }

    $scope.loadMore = function(){
        $scope.page_no += 1;
        $scope.getDrsList();
    }


    $scope.getDliveredList = function () {
        $scope.drs_list.user_id = $scope.current_user[0].id;
        $http({
            url: BASEURL + '/api.php?page='+$scope.page_no+'&method_name=delivered_list',
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: $scope.drs_list
        }).success(function (response){
            if(response.length > 0 && response != null){
                angular.forEach(response,function(value){
                    if(value.delivered == 'Y'){
                        $scope.my_drs.push(value);
                    }
                });
                $scope.$broadcast('scroll.infiniteScrollComplete');
            }else{
               $scope.stopLoader = true;
            }
            $ionicLoading.hide();
        });
    }

    $scope.loadDeliveredMore = function(){
        $scope.page_no += 1;
        $scope.getDliveredList();
    }


        $scope.searchDrsList = function(event){
            event.preventDefault();
            $scope.my_drs = [];
            $scope.page_no = 1;
            $ionicLoading.show({
                template: '<ion-spinner icon="ios"></ion-spinner>'
            });
            $scope.getDrsList();
        }



        $scope.searchDeliveredList = function(event){
            event.preventDefault();
            $scope.my_drs = [];
            $scope.page_no = 1;
            $ionicLoading.show({
                template: '<ion-spinner icon="ios"></ion-spinner>'
            });
            $scope.getDliveredList();
        }
});

app.controller('DrsDetailCtrl', function ($scope,$ionicLoading, $http, BASEURL, $window, $stateParams,$q) {
            $scope.current_user = JSON.parse($window.sessionStorage["userInfo"]);
            if ($stateParams.id) {
                $scope.pickup_detail = {
                    booking_id: $stateParams.id,
                    user_id: $scope.current_user[0].id
                };
            }
            $scope.search = '';
            $scope.awb_list = [];
            $scope.page_no = 1;
//            $scope.$on('$ionicView.beforeEnter', function() {
//                $scope.search = '';
//                $scope.awb_list = [];
//                $scope.page_no = 1;
//                // Code you want executed every time view is opened
//                $scope.getDrsDetail();
//            });

$ionicLoading.show({
                template: '<ion-spinner icon="ios"></ion-spinner>'
            });

            $scope.getDrsDetail = function () {

                $http({
                    url: BASEURL + '/api.php?page='+$scope.page_no+'&search='+$scope.search+'&drs_id=' + $stateParams.id + '&method_name=drs_detail&type='+$stateParams.type+'&user_id=' + $scope.current_user[0].id,
                    method: "GET",
                    data: $scope.pickup_detail
                }).success(function (response) {
                    if(response.length > 0){
                        angular.forEach(response,function(value){
                           $scope.awb_list.push(value);
                        });
                        $scope.$broadcast('scroll.infiniteScrollComplete');
                    }else{
                       $scope.stopLoader = true;
                    }
                    $ionicLoading.hide();
                });
            }

            $scope.loadMore = function(){
                $scope.page_no += 1;
                $scope.getDrsDetail();
            }
});



app.controller('SmartPickupCtrl', function ($scope,$ionicPopup,NgMap,$rootScope,$ionicLoading,$state, $http, BASEURL, $window, $stateParams) {
  $rootScope.logLatLng = function(e) {
          console.log('loc', e.latLng);
        }



	 $ionicLoading.show({
                template: '<ion-spinner icon="ios"></ion-spinner>'
            });
		navigator.geolocation.getCurrentPosition(function(position) {
$scope.Latitude=position.coords.latitude
$scope.Longitude=position.coords.longitude


if($scope.Latitude!=null)
{
$scope.smart_booking_route();
$ionicLoading.hide();

}
})
    $scope.$on('$ionicView.beforeEnter', function() {
        if($window.sessionStorage["userInfo"] != undefined && $window.sessionStorage["userInfo"] != '' && $window.sessionStorage["userInfo"] != 'false'){

            $scope.current_user = JSON.parse($window.sessionStorage["userInfo"]);
            if ($stateParams.id) {
                $scope.user_lat = $rootScope.current_lat ;
                $scope.user_lng =  $rootScope.current_lng;

                $scope.pickup_start_lat = $rootScope.current_lat;
                $scope.pickup_start_lng = $rootScope.current_lng;

                $scope.pickup_end_lat = $rootScope.current_lat;
                $scope.pickup_end_lng = $rootScope.current_lng;

                $scope.path = [];
            }
        }else{
            $state.go('auth.login');
        }
    });

    $scope.smart_booking_route = function(){
        $http({
            url: BASEURL + '/api.php?pickup_id=' + $stateParams.id + '&method_name=smart_pickup_detail&user_id=' + $scope.current_user[0].id,
            method: "GET",
            data: $scope.pickup_detail
        }).success(function (response) {
            $scope.data = response;

            if(response){
			$scope.data=response;
            $scope.origin= $scope.data[0].address;
			 $scope.destination= $scope.data[(response.length-1)].address;


                angular.forEach(response, function(value, key) {
                    var add = [];
                    if(value){

                            $scope.path.push({location:value.address , stopover: true});


                    }
                });

            }


        });
    }

    $scope.openPikcupDetailPage = function(e,shipment){
         $scope.shipment_detail = shipment;
        this.map.showInfoWindow('ship', shipment.slip_no);
//        $state.go('app.booking-detail',{id:this.id});
    }






});






app.controller('SmartDeliveryCtrl', function ($scope,$ionicPopup,NgMap,$rootScope,$ionicLoading,$state, $http, BASEURL, $window, $stateParams) {


  $rootScope.logLatLng = function(e) {
          console.log('loc', e.latLng);
        }



	 $ionicLoading.show({
                template: '<ion-spinner icon="ios"></ion-spinner>'
            });
		navigator.geolocation.getCurrentPosition(function(position) {
$scope.Latitude=position.coords.latitude
$scope.Longitude=position.coords.longitude


if($scope.Latitude!=null)
{
$scope.smart_booking_route();
$ionicLoading.hide();

}
})
    $scope.$on('$ionicView.beforeEnter', function() {
        if($window.sessionStorage["userInfo"] != undefined && $window.sessionStorage["userInfo"] != '' && $window.sessionStorage["userInfo"] != 'false'){
            $scope.current_user = JSON.parse($window.sessionStorage["userInfo"]);
            if ($stateParams.id) {
                $scope.user_lat = $rootScope.current_lat ;
                $scope.user_lng =  $rootScope.current_lng;

                $scope.pickup_start_lat = $rootScope.current_lat;
                $scope.pickup_start_lng = $rootScope.current_lng;

                $scope.pickup_end_lat = $rootScope.current_lat;
                $scope.pickup_end_lng = $rootScope.current_lng;

                $scope.path = [];
            }
        }else{
            $state.go('auth.login');
        }
    });

    $scope.smart_booking_route = function(){
        $http({
            url: BASEURL + '/api.php?pickup_id=' + $stateParams.id + '&method_name=smart_delivery_detail&user_id=' + $scope.current_user[0].id,
            method: "GET",
            data: $scope.pickup_detail
        }).success(function (response) {
            $scope.data = response;

            if(response){
			$scope.data=response;
            $scope.origin= $scope.data[0].address;
			 $scope.destination= $scope.data[(response.length-1)].address;


                angular.forEach(response, function(value, key) {
                    var add = [];
                    if(value){

                            $scope.path.push({location:value.address , stopover: true});


                    }
                });

            }


        });
    }

    $scope.openPikcupDetailPage = function(e,shipment){
         $scope.shipment_detail = shipment;
        this.map.showInfoWindow('ship', shipment.slip_no);
//        $state.go('app.booking-detail',{id:this.id});
    }








	});

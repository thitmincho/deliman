// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
//http://192.168.1.10/safetoship_app/
angular.module('starter', ['ionic', 'starter.controllers'])
//.constant('BASEURL', 'http://192.168.1.10/demo_courier_v6/')
.constant('BASEURL', 'http://waaneizadigitalmarket.com/api2')
.factory("LocalStorage", function ($window, $rootScope) {
    return {
        setData: function (val) {
            $window.localStorage && $window.localStorage.setItem('logged_in_user', val);
            return this;
        },
        getData: function () {
            return $window.localStorage && $window.localStorage.getItem('logged_in_user');
        }
    };
})

.directive('numbersOnly', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                if (text) {
                    var transformedInput = text.replace(/[^0-9]/g, '');

                    if (transformedInput !== text) {
                        ngModelCtrl.$setViewValue(transformedInput);
                        ngModelCtrl.$render();
                    }
                    return transformedInput;
                }
                return undefined;
            }
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
})
.service('UserService', function ($http,BASEURL) {
    var user = this;

    /*
     * function to get a list of religions
     */
    user.getCountryList = function () {
        return $http.get(BASEURL + 'andr_user.php?method_name=country_list').success(function (response) {
            return response;
        });
    }


    /*
     * function to get a list of users by leader uid
     */
    user.getCityList = function (country) {
        return $http.get(BASEURL + 'andr_user.php?method_name=city_list&country='+country).success(function (response) {
            return response;
        });
    }



})
.service('TrackingService', function ($http,BASEURL) {
    var track = this;

    /*
     * function to get a list of religions
     */
    track.getTrackingDetail = function (tracking) {
        return $http({
                    method: "POST",
                    url: BASEURL + "/trackapi.php?section=tracking",
                    data: tracking,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function (response) {
            return response;
        });
    }


})
.run(function($ionicPlatform,$rootScope,BASEURL,$window,$http) {



	/*$ionicPlatform.ready(function() {
    var push = new Ionic.Push({
      "debug": true



    });

    push.register(function(token) {
      console.log("Device token:",token.token);
      push.saveToken(token);
	  console.log('my tokan:'+token.token);
	   var current_user = JSON.parse($window.sessionStorage["userInfo"]);
	   console.log(current_user.id);

	   $http({
                    method: "POST",
                    url: BASEURL + "/andr_user.php?method_name=updateTokan",
                    data: {
						id:current_user.id,
						TOKAN:token.token
						},
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function (response) {
            return response;
        });


	   // persist the token in the Ionic Platform
    });
  });*/

    $rootScope.BASEURL = BASEURL;
  $ionicPlatform.ready(function(){
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    //window.plugins.OneSignal.setLogLevel({logLevel: 4, visualLevel: 4});
    var notificationOpenedCallback = function(jsonData) {
    console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
    };

    window.plugins.OneSignal
    .startInit("176495ab-afc6-4167-9b68-e7253ad87b6d")
    .handleNotificationOpened(notificationOpenedCallback)
    .endInit();

    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }

  });

})

.config(function($stateProvider, $urlRouterProvider) {




  $stateProvider

  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })


  .state('auth', {
    url: '/auth',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

   .state('auth.register', {
    url: '/register',
    views: {
      'menuContent': {
        templateUrl: 'templates/register.html',
        controller:  'RegisterCtrl'
      }
    }
  })

  .state('app.contact', {
    url: '/contact',
    views: {
      'menuContent': {
        templateUrl: 'templates/contact.html',
        //controller:  'ProfileCtrl'
      }
    }
  })

 .state('auth.login', {
    url: '/login',
    views: {
      'menuContent': {
        templateUrl: 'templates/login.html',
        controller: 'LoginCtrl'

      }
    }
  })
 .state('auth.forgot-password', {
    url: '/forgot-password',
    views: {
      'menuContent': {
        templateUrl: 'templates/forgot-password.html',
        controller: 'ForgotPasswordCtrl'

      }
    }
  })

 /* .state('app.contact', {
    url: '/contact',
    views: {
      'menuContent': {
        templateUrl: 'templates/contact.html',
        //controller:  'ProfileCtrl'
      }
    }
  })  */

.state('app.booking-step-1', {
      url: '/booking-step-1',
      views: {
        'menuContent': {
          templateUrl: 'templates/booking-step-1.html',
         // controller: 'bookingCtrl'
        }
      }
    })

	.state('app.booking-step-2', {
      url: '/booking-step-2',
      views: {
        'menuContent': {
          templateUrl: 'templates/booking-step-2.html',
         // controller: 'bookingCtrl'
        }
      }
    })

.state('app.booking-step-2-ksa', {
      url: '/booking-step-2-ksa',
      views: {
        'menuContent': {
          templateUrl: 'templates/booking-step-2-ksa.html',
         // controller: 'bookingCtrl'
        }
      }
    })


	.state('app.signature', {
      url: '/signature',
      views: {
        'menuContent': {
          templateUrl: 'templates/signature.html',
         // controller: 'bookingCtrl'
        }
      }
    })

    .state('app.booking-step-2-int', {
      url: '/booking-step-2-int',
      views: {
        'menuContent': {
          templateUrl: 'templates/booking-step-2-int.html',
         // controller: 'bookingCtrl'
        }
      }
    })

 .state('app.dashboard', {
    url: '/dashboard',
    views: {
      'menuContent': {
        templateUrl: 'templates/dashboard.html',
        controller: 'HomeCtrl'
      }
    }
  })

  .state('app.newpage', {
    url: '/newpage',
    views: {
      'menuContent': {
        templateUrl: 'templates/newpage.html',
        controller: 'newpageCltr'
      }
    }
  })

  .state('app.contact-us', {
    url: '/contact-us',
    views: {
      'menuContent': {
        templateUrl: 'templates/contact-us.html',
        controller: 'ContactUsCtrl'
      }
    }
  })

    .state('app.tracking-detail', {
    url: '/tracking-detail/:tracking_no',
    views: {
      'menuContent': {
        templateUrl: 'templates/tracking-detail.html',
        controller: 'trackCltr'
      }
    }
  })
  .state('app.pickup-detail', {
    url: '/pickup-detail/:id/:type',
    views: {
      'menuContent': {
        templateUrl: 'templates/pickup-detail.html',
        controller: 'PickupDetailCtrl'
      }
    }
  })

.state('app.smart-pickup', {
    url: '/smart-pickup/:id',
    views: {
      'menuContent': {
        templateUrl: 'templates/smart-pickup.html',
        controller: 'SmartPickupCtrl'
      }
    }
  })

  .state('app.smart-delivery', {
    url: '/smart-delivery/:id',
    views: {
      'menuContent': {
        templateUrl: 'templates/smart-delivery.html',
        controller: 'SmartDeliveryCtrl'
      }
    }
  })


  .state('app.booking-detail', {
    url: '/booking-detail/:id',
    views: {
      'menuContent': {
        templateUrl: 'templates/booking-detail.html',
        controller: 'BookingDetailCtrl'
      }
    }
  })


    .state('app.drs-booking-detail', {
    url: '/drs-booking-detail/:id',
    views: {
      'menuContent': {
        templateUrl: 'templates/drs-booking-detail.html',
        controller: 'BookingDetailCtrl'
      }
    }
  })

  .state('app.signature-pad', {
    url: '/signature-pad/:id',
    views: {
      'menuContent': {
        templateUrl: 'templates/signature-pad.html',
        controller: 'SignCtrl'
      }
    }
  })


   .state('app.pickup-history',{
    url: '/pickup-history',
    views:{
      'menuContent': {
        templateUrl: 'templates/pickup-history.html',
        controller: 'BookingListCtrl'
      }
    }
  })
   .state('app.news-detail', {
    //url: '/news-detail/:id',
    url: '/news-detail',
    views: {
      'menuContent': {
        templateUrl: 'templates/news-detail.html',
        controller: 'NewsCtrl'
      }
    }
  })
   .state('app.delivered', {
    url: '/delivered',
    views: {
      'menuContent': {
        templateUrl: 'templates/delivered.html',
        controller: 'DrsListCtrl'
      }
    }
  })
  .state('app.search', {
    url: '/search',
    views: {
      'menuContent': {
        templateUrl: 'templates/search.html'
      }
    }
  })
  .state('app.profile', {
    url: '/profile',
    views: {
      'menuContent': {
        templateUrl: 'templates/profile.html',
        controller:  'ProfileCtrl'
      }
    }
  })

.state('app.booking_list', {
    url: '/booking_list',
    views: {
      'menuContent': {
        templateUrl: 'templates/booking_list.html',
        controller:  'BookingListCtrl'
      }
    }
  })


  .state('app.browse', {
      url: '/browse',
      views: {
        'menuContent': {
          templateUrl: 'templates/browse.html'
        }
      }
    })


  .state('app.single', {
    url: '/playlists/:playlistId',
    views: {
      'menuContent': {
        templateUrl: 'templates/playlist.html',
        controller: 'PlaylistCtrl'
      }
    }
  })

.state('app.drs-list', {
    url: '/drs-list',
    views: {
      'menuContent': {
        templateUrl: 'templates/drs_list.html',
        controller: 'DrsListCtrl'
      }
    }
})

.state('app.drs-detail', {
    url: '/drs-detail/:id/:type',
    views: {
      'menuContent': {
        templateUrl: 'templates/drs-detail.html',
        controller: 'DrsDetailCtrl'
      }
    }
})


//.state('app.mylocation', {
//    url: '/mylocation',
//    views: {
//      'menuContent': {
//        templateUrl: 'templates/mylocation.html',
//        controller: 'MyLocCtrl'
//      }
//    }
//})


.state('app.profile-update', {
        url: '/profile-update',
        views: {
          'menuContent': {
            templateUrl: 'templates/profile-update.html',
            controller: 'ProfileCtrl'
          }
        }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/auth/login');
})
